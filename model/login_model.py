from selenium.webdriver.common.by import By
from fwtest.webdriver_wrapper import get_driver

driver = get_driver()
NAME_ID = (By.ID, 'login-subject')
PASSWORD_ID = (By.ID, 'login-password')
BUTTON_XPATH = (By.XPATH, '//button[contains(@class,"btn-input")]')

class LoginModel:
    def __init__(self):
        self.driver = driver
        if self.driver.is_logged():
            self.driver.log_out()
            self.driver.get_page(url='/login')

    def send_name(self, name):
        """
        Метод вводит заданное имя в поле логин
        """
        login = self.driver.get_element(*NAME_ID)
        login.clear()
        login.send_keys(name)

    def send_password(self, password):
        """
        Метод вводит пароль
        """
        user_pass = self.driver.get_element(*PASSWORD_ID)
        user_pass.clear()
        user_pass.send_keys(password)

    def login_button_click(self):
        """
        Метод нажимает на кнопку "Войти"
        """        
        button_click = self.driver.get_element(*BUTTON_XPATH)
        button_click.click()
        if self.driver.get_current_url().count('testone.imgbb.com') == 1:
            return True
        else:
            return False



