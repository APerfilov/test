1. Скачайте драйвер для Chrome из https://sites.google.com/a/chromium.org/chromedriver/downloads и установите его в своё окружение.
2. Создайте свою виртуальную среду и установите необходимые пакеты:
        `pip3 install -r requirements.txt`
3. Запустите тесты:
        `pytest ./tests -v`
