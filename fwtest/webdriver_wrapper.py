from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as ec
import os


"""
Тут использую параметры заданные напрямую.
В случае с большим разнообразием url и параметров
предполагается использовать обработчик для получения 
настроек из файла.
"""
work_url = 'https://imgbb.com/'
time = 10

"""
Задаются опции браузера
"""
headless = True
log_level = 'debug'

if headless:
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument("no-sandbox")

    if log_level == 'debug':
        driver = webdriver.Chrome(options=options, service_args=["--verbose", "--log-path=./logs/selenium.log"])
    else:
        driver = webdriver.Chrome(options=options)

    driver.set_window_size(1920, 1080)

else:
    driver = webdriver.Chrome()
    driver.maximize_window()


DRIVER = None

class __Wrapper:
    def __init__(self):
        self.work_url = work_url
        self.driver = driver

    def get_element(self, *loc):
        """
        Реализует поиск элемента по локатору
        :param locator: локатор
        :return:
        """
        try:

            return self.driver.find_element(*loc)

        except NoSuchElementException:
            return None

    def log_out(self):
        """
        Осуществляет выход из приложения
        :return:
        """
        driver.get('https://imgbb.com/logout')

    def get_current_url(self):
        """
        Получение текущего url
        :return:
        """
        return self.driver.current_url

    def get_page(self, url):
        """
        Переход на страницу по url
        """
        url = self.work_url + url
        url.replace('//', '/')
        self.driver.get(url)


    def wait_element(self, by, elem, time=None):
        """
        Ожидание загрузки элемента
        :param by:
        :param elem:
        :param await_time:
        :return:
        """
        if time is None:
            time = 20
        result = True
        try:
            WebDriverWait(self.driver, time).until((ec.visibility_of_element_located((by, elem))))
        except TimeoutException:
            result = False
        return result
        
    def is_logged(self):
            """
            Проверяет выполнен ли вход в MDS
            :return: True - если вход выполнен, False - если нет
            """
            if self.driver.current_url.count(f'{work_url}/login') == 1:
                return False
            return True


    def __del__(self):
        self.driver.quit()


def get_driver():
    global DRIVER
    if DRIVER is not None:
        return DRIVER
    else:
        DRIVER = __Wrapper()
        DRIVER.driver.get(work_url)
        return DRIVER
