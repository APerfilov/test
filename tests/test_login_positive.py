import pytest
import allure
from model.login_model import LoginModel

driver = LoginModel()

def test_login_valid():
    """
    Тест проверяет попытку входа с валидными логином и паролем
    """
    driver.send_name('testone')
    driver.send_password('123456')
    result = driver.login_button_click()
    with allure.step('Проверка, что вход выполнен'):
        assert result, 'Вход не выполнен после нажатия кнопки "Войти"'
